#!/usr/bin/env python
# -*- coding: utf-8 -*-



def datos(covid19):
    for i, linea in enumerate(covid19):
        lista = linea.replace('.0', '').split(',')#separar datos
        if lista[5] == '0':
            continue
        datos = lista[3:6:2]
        print(', '.join(datos))#string

def pasar(covid19):
    with open('covid_19_data.csv', 'r') as f:
        lineas = [linea.split(',') for linea in f]

    for linea in lineas:
        print(linea[3], linea[5])#presenta las líneas solicitadas sin mayor arreglo
#def ordenar países (guardar paises en lista mientras ek país se repita solo sumar los datos)
#para presentar países sin repeticiones
if __name__ == '__main__':
    covid19 = open('covid_19_data.csv', 'r')
    datos(covid19)
    pasar(covid19)
    covid19.close()
