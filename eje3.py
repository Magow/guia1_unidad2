#!/usr/bin/env python
# -*- coding: utf-8 -*-

aminos_form = {'Glicina':'C2H5NO2','Alanina':'C3H7NO2',
            'Valina':'C5H11NO2','Leucina':'C6H13NO2',
            'Isoleucina':'C6H13NO2', 'Fenilalanina':'C9H11NO2',
            'Tirosina':'C9H11NO3', 'Triptofano': 'C11H12N2O2',
            'Serina':'C3H7NO3', 'Treonina':'C4H9NO3',
            'Cisteina':'C3H7NO2S','Metionina':'C5H11NO2S',
            'Acido aspartámico':'C4H7NO4', 'Acido glutamico':'C5H9NO4',
            'Histidina':'C6H9N3O2', 'Lisina':'C6H14N2O2',
            'Arginina':'C6H14N4O2', 'Asparginina':'C4H8N2O3',
            'Glutamina':'C5H10N2O3', 'Prolina':'C5H9NO2'}

aminos = {}
option = ''
def menu():
    option = ''
    print('Menú de opciones')
    print('(1) Añadir Aminoacido')
    print('(2) Eliminar Aminoacido')
    print('(3) Mostrar Aminoacido')
    print('(4) Listar Aminoacidos ingresados')
    print('(5) Listar Aminoacidos ORIGINALES')
    print('(6) Terminar')
    option = input('Elige una opción:')


    while option != '6':

        if option == '1':#agregar aminoácido y validarlo para no agregar datos inútiles
            n_amin = input('Introduce el nombre del aminoacido: ')
            name = input('Introduce la formula: ')
            amino = {'nombre':name}
            aminos[n_amin] = amino
            if n_amin in aminos_form:
                print ("aminoácido correcto")
            else:
                print ("Ese aminoácido no existe, escríbalo nuevamemte")
            menu()

        if option == '2':#eliminación
            n_amin = input('Introduce el nombre del amonoácido ingresado: ')
            if n_amin in aminos:
                del aminos[n_amin]
            else:
                print('No existe el nombre de este aminoacido', n_amin)
            menu()

        if option == '3':#mostrar fórmula de aminoácido
            n_amin = input('Ingrese nombre de aminoacido: ')
            if n_amin in aminos:
                print('Nombre de aminoácido:', n_amin)
                for key, value in aminos[n_amin].items():
                    print(key.title() + ':', value)
            else:
                print('No existe el aminoacido con este nombre ', n_amin)
            menu()

        if option == '4':#mostrar todos los aminoácidos ingresados
            print('Lista de Aminoacidos')
            for key, value in aminos.items():
                print(key, value['nombre'])
            menu()

        if option == '5':#mostrar los 20 aminoácidos
            print('Lista de Aminoacidos ORIGINALES')
            for i,j in aminos_form.items():
                print ("%s -> %s" %(i,j))
        else:
            break
        menu()

if __name__ == '__main__':
    menu()
